package com.kiliaro.test.di

import android.content.Context
import com.kiliaro.test.data.local.db.AppDatabase
import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaDBDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return AppDatabase.getDatabase(context)
    }

    @Provides
    fun provideSharedMediaDao(appDatabase: AppDatabase): SharedMediaDBDataSource {
        return appDatabase.sharedMediaDAO()
    }
}