package com.kiliaro.test.di

import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaDBDataSource
import com.kiliaro.test.data.remote.api.sharedMedia.SharedMediaApiDataSource
import com.kiliaro.test.data.repository.SharedMediaRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Singleton
    @Provides
    fun provideSharedMediaRepository(
        dbDataSource: SharedMediaDBDataSource,
        apiDataSource: SharedMediaApiDataSource
    ): SharedMediaRepository {
        return SharedMediaRepository(dbDataSource, apiDataSource)
    }
}