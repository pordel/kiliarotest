package com.kiliaro.test.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.kiliaro.test.R
import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaEntity
import com.kiliaro.test.databinding.ItemSharedMediaBinding
import com.kiliaro.test.util.AppUtils
import kotlin.math.roundToInt

class SharedMediaAdapter(private val onClick: (SharedMediaEntity) -> Unit) :
    ListAdapter<SharedMediaEntity, SharedMediaAdapter.SharedMediaViewHolder>(SharedMediaDiffCallback) {

    class SharedMediaViewHolder(
        private val binding: ItemSharedMediaBinding,
        val onClick: (SharedMediaEntity) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        private var currentSharedMedia: SharedMediaEntity? = null

        private val displayWidth = AppUtils.getScreenWidth(itemView.context)
        private val columnCount = 3
        private var columnWidth = (displayWidth / columnCount.toDouble()).roundToInt()

        init {
            itemView.setOnClickListener {
                currentSharedMedia?.let {
                    onClick(it)
                }
            }
        }

        fun bind(sharedMedia: SharedMediaEntity) {
            currentSharedMedia = sharedMedia

            currentSharedMedia?.let {
                val fileSize = AppUtils.bytesToMegaByte(it.size)
                binding.tvImageSize.text = fileSize

                val url = "${it.thumbnailUrl}?h=$columnWidth&w=$columnWidth&m=crop"
                Glide.with(itemView.context)
                    .load(url)
                    .transition(withCrossFade())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.img_placeholder)
                    .override(columnWidth, columnWidth)
                    .into(binding.ivImage)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SharedMediaViewHolder {
        val binding =
            ItemSharedMediaBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SharedMediaViewHolder(binding, onClick)
    }

    override fun onBindViewHolder(holder: SharedMediaViewHolder, position: Int) {
        val media = getItem(position)
        holder.bind(media)
    }

}

object SharedMediaDiffCallback : DiffUtil.ItemCallback<SharedMediaEntity>() {
    override fun areItemsTheSame(oldItem: SharedMediaEntity, newItem: SharedMediaEntity): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: SharedMediaEntity,
        newItem: SharedMediaEntity
    ): Boolean {
        return oldItem.mediaId == newItem.mediaId
    }
}
