package com.kiliaro.test.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.kiliaro.test.R
import com.kiliaro.test.data.base.DataState
import com.kiliaro.test.data.base.NetworkErrorType
import com.kiliaro.test.databinding.MainFragmentBinding
import com.kiliaro.test.util.AppUtils
import com.kiliaro.test.util.NetworkUtil
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel

    private lateinit var sharedMediaAdapter: SharedMediaAdapter

    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!

    private var errorSnackBar: Snackbar? = null

    override fun onDestroyView() {
        super.onDestroyView()
        errorSnackBar?.dismiss()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.title =
            AppUtils.getString(requireContext(), R.string.app_name)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        setupUI()
        fetchData()
        setupObserver()
    }

    private fun setupUI() {
        sharedMediaAdapter = SharedMediaAdapter {
            findNavController().navigate(
                MainFragmentDirections.actionMainFragmentToDetailsFragment(
                    it.thumbnailUrl,
                    it.createdAt
                )
            )
        }

        binding.recyclerViewGallery.apply {
            layoutManager = GridLayoutManager(requireContext(), 3)
            setHasFixedSize(true)
            adapter = sharedMediaAdapter
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            MaterialAlertDialogBuilder(requireContext(), R.style.CustomMaterialDialogStyle)
                .setTitle(resources.getString(R.string.refresh_data))
                .setMessage(resources.getString(R.string.refresh_data_warning))
                .setNeutralButton(resources.getString(R.string.decline)) { dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton(resources.getString(R.string.accept)) { dialog, _ ->
                    dialog.dismiss()
                    clearCache()
                }
                .show()
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun fetchData() {
        lifecycleScope.launchWhenStarted {
            viewModel.getSharedMedia(!NetworkUtil.isNetworkAvailable(requireContext()))
        }
    }

    private fun clearCache() {
        lifecycleScope.launchWhenStarted {
            viewModel.clearCache(requireContext())
        }
    }

    private fun setupObserver() {
        viewModel.sharedMediaLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is DataState.Loading -> {
                    showLoading(show = true)
                }
                is DataState.Success -> {
                    showLoading(show = false)
                    sharedMediaAdapter.submitList(it.value)
                }
                is DataState.LocalError -> {
                    showError(
                        errorMessage = it.message ?: AppUtils.getString(
                            requireContext(),
                            R.string.local_error
                        ),
                        buttonText = AppUtils.getString(requireContext(), R.string.retry)
                    )
                }
                is DataState.NetworkError -> {
                    when (it.networkError) {
                        is NetworkErrorType.NetworkConnection -> {
                            showError(
                                errorMessage = AppUtils.getString(
                                    requireContext(),
                                    R.string.no_internet
                                ),
                                buttonText = AppUtils.getString(requireContext(), R.string.retry)
                            )
                        }
                        is NetworkErrorType.Unknown -> {
                            showError(
                                errorMessage = it.networkError.message ?: AppUtils.getString(
                                    requireContext(),
                                    R.string.unknown_error
                                ),
                                buttonText = AppUtils.getString(requireContext(), R.string.retry)
                            )
                        }
                        else -> {
                            showError(
                                errorMessage = AppUtils.getString(
                                    requireContext(),
                                    R.string.server_error
                                ),
                                buttonText = AppUtils.getString(requireContext(), R.string.retry)
                            )
                        }
                    }

                }
            }
        })

        viewModel.clearCacheLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is DataState.Loading -> {
                    showLoading(show = true)
                }
                is DataState.Success -> {
                    viewModel.getSharedMedia(!NetworkUtil.isNetworkAvailable(requireContext()))
                }
                else -> {
                    showError(
                        errorMessage = AppUtils.getString(
                            requireContext(),
                            R.string.local_error
                        ),
                        buttonText = AppUtils.getString(requireContext(), R.string.retry)
                    )
                }
            }
        })
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            binding.recyclerViewGallery.visibility = View.GONE
            binding.progress.visibility = View.VISIBLE
        } else {
            binding.recyclerViewGallery.visibility = View.VISIBLE
            binding.progress.visibility = View.GONE
        }
    }

    private fun showError(errorMessage: String, buttonText: String) {
        binding.progress.visibility = View.GONE
        errorSnackBar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
            .setAction(buttonText) {
                fetchData()
            }
            .setBackgroundTint(AppUtils.getColor(requireContext(), R.color.errorSnackBarBackground))
            .setActionTextColor(AppUtils.getColor(requireContext(), R.color.errorSnackBarText))
            .setTextColor(AppUtils.getColor(requireContext(), R.color.errorSnackBarText))
        errorSnackBar?.show()
    }

}