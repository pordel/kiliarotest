package com.kiliaro.test.ui.details

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.snackbar.Snackbar
import com.kiliaro.test.R
import com.kiliaro.test.databinding.DetailsFragmentBinding
import com.kiliaro.test.util.AppUtils
import com.kiliaro.test.util.NetworkUtil
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : Fragment() {

    private lateinit var viewModel: DetailsViewModel
    private val args: DetailsFragmentArgs by navArgs()

    private var _binding: DetailsFragmentBinding? = null
    private val binding get() = _binding!!

    private var errorSnackBar: Snackbar? = null

    override fun onDestroyView() {
        super.onDestroyView()
        errorSnackBar?.dismiss()
        Glide.with(this).clear(binding.ivFullImage)
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this)[DetailsViewModel::class.java]

        viewModel.createdAt = args.createdAt
        viewModel.imageUrl = args.imageUrl

        setupUI()
    }

    private fun setupUI() {
        viewModel.createdAt?.let {
            val date = AppUtils.convertDateFormat(it)
            if (date.isNotEmpty()) {
                (activity as AppCompatActivity).supportActionBar?.title =
                    AppUtils.convertDateFormat(it)
            }
        }
        loadImage()
    }

    private fun loadImage() {
        viewModel.imageUrl?.let {
            showProgress(true)
            val url = "${it}?w=${AppUtils.getScreenWidth(requireContext())}"
            Glide.with(requireContext())
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: Target<Drawable>,
                        isFirstResource: Boolean
                    ): Boolean {
                        showError(
                            errorMessage = AppUtils.getString(
                                requireContext(),
                                if (NetworkUtil.isNetworkAvailable(requireContext()))
                                    R.string.load_image_error else R.string.no_internet
                            ),
                            buttonText = AppUtils.getString(
                                requireContext(),
                                R.string.retry
                            ),
                        )
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        model: Any,
                        target: Target<Drawable>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        showProgress(false)
                        return false
                    }
                })
                .into(binding.ivFullImage)
        }
    }


    private fun showProgress(show: Boolean) {
        if (show) {
            binding.progress.visibility = View.VISIBLE
            binding.ivFullImage.visibility = View.INVISIBLE
        } else {
            binding.progress.visibility = View.GONE
            binding.ivFullImage.visibility = View.VISIBLE
        }
    }

    private fun showError(errorMessage: String, buttonText: String) {
        binding.progress.visibility = View.GONE
        errorSnackBar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
            .setAction(buttonText) {
                loadImage()
            }
            .setBackgroundTint(AppUtils.getColor(requireContext(), R.color.errorSnackBarBackground))
            .setActionTextColor(AppUtils.getColor(requireContext(), R.color.errorSnackBarText))
            .setTextColor(AppUtils.getColor(requireContext(), R.color.errorSnackBarText))
        errorSnackBar?.show()
    }

}