package com.kiliaro.test.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kiliaro.test.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}