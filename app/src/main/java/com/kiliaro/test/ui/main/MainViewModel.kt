package com.kiliaro.test.ui.main

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kiliaro.test.data.base.DataState
import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaEntity
import com.kiliaro.test.data.repository.SharedMediaRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: SharedMediaRepository
) : ViewModel() {

    private val _sharedMediaLiveData: MutableLiveData<DataState<List<SharedMediaEntity>>> =
        MutableLiveData()
    val sharedMediaLiveData: LiveData<DataState<List<SharedMediaEntity>>> = _sharedMediaLiveData

    private val _clearCacheLiveData: MutableLiveData<DataState<Boolean>> =
        MutableLiveData()
    val clearCacheLiveData: LiveData<DataState<Boolean>> = _clearCacheLiveData

    fun getSharedMedia(offlineMode : Boolean) {
        viewModelScope.launch {
            _sharedMediaLiveData.value = DataState.Loading
            repository.getSharedMedia(offlineMode)
                .catch {
                    _sharedMediaLiveData.value = DataState.LocalError()
                }
                .collect {
                    _sharedMediaLiveData.value = it
                }
        }
    }

    fun clearCache(context: Context) {
        viewModelScope.launch {
            _clearCacheLiveData.value = DataState.Loading
            repository.clearData(context)
                .catch {
                    _clearCacheLiveData.value = DataState.LocalError()
                }
                .collect {
                    _clearCacheLiveData.value = it
                }
        }
    }

}