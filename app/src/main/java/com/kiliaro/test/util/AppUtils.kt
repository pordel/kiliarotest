package com.kiliaro.test.util

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class AppUtils {
    companion object {
        fun getScreenHeight(context: Context): Int {
            return context.resources.displayMetrics.heightPixels
        }

        fun getScreenWidth(context: Context): Int {
            return context.resources.displayMetrics.widthPixels
        }

        fun getString(context: Context, resourceId: Int): String {
            return context.resources.getString(resourceId)
        }

        fun getColor(context: Context, resourceId: Int): Int {
            return ContextCompat.getColor(context, resourceId)
        }

        fun getDrawable(context: Context, resourceId: Int): Drawable? {
            return ContextCompat.getDrawable(context, resourceId)
        }

        fun convertDateFormat(date: String): String {
            return try {
            val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
                DateFormat.getDateTimeInstance().format(format.parse(date)!!)
            } catch (e: java.text.ParseException) {
                ""
            }
        }

        fun bytesToMegaByte(size: Int): String {
            val kb: Double = size.toDouble() / 1024
            val mb: Double = kb / 1024
            return String.format("%.2f", mb) + " MB"
        }

    }
}