package com.kiliaro.test.data.mapper

import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaEntity
import com.kiliaro.test.data.remote.api.sharedMedia.SharedMediaResponse

object SharedMediaMapper {
    fun map(dto: List<SharedMediaResponse>): List<SharedMediaEntity> {
        return dto.map {
            SharedMediaEntity(
                mediaId = it.mediaId,
                userId = it.userId,
                mediaType = it.mediaType,
                filename = it.filename,
                size = it.size,
                createdAt = it.createdAt,
                takenAt = it.takenAt,
                guessedTakenAt = it.guessedTakenAt,
                md5sum = it.md5sum,
                contentType = it.contentType,
                video = it.video,
                thumbnailUrl = it.thumbnailUrl,
                downloadUrl = it.downloadUrl,
                resX = it.resX,
                resY = it.resY,
            )
        }
    }
}