package com.kiliaro.test.data.local.db.sharedMedia

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "sharedMedia")
data class SharedMediaEntity(
    @ColumnInfo(name = "media_id")
    val mediaId: String,

    @ColumnInfo(name = "user_id")
    val userId: String,

    @ColumnInfo(name = "media_type")
    val mediaType: String,

    @ColumnInfo(name = "filename")
    val filename: String,

    @ColumnInfo(name = "size")
    val size: Int,

    @ColumnInfo(name = "created_at")
    val createdAt: String,

    @ColumnInfo(name = "taken_at")
    val takenAt: String?,

    @ColumnInfo(name = "guessed_taken_at")
    val guessedTakenAt: String?,

    @ColumnInfo(name = "md5sum")
    val md5sum: String,

    @ColumnInfo(name = "content_type")
    val contentType: String,

    @ColumnInfo(name = "video")
    val video: String?,

    @ColumnInfo(name = "thumbnail_url")
    val thumbnailUrl: String,

    @ColumnInfo(name = "download_url")
    val downloadUrl: String,

    @ColumnInfo(name = "resx")
    val resX: Int,

    @ColumnInfo(name = "resy")
    val resY: Int
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0
}