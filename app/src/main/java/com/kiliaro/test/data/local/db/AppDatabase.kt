package com.kiliaro.test.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kiliaro.test.AppConstants
import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaDBDataSource
import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaEntity

@Database(entities = [SharedMediaEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun sharedMediaDAO(): SharedMediaDBDataSource

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    AppConstants.DATABASE_NAME
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}


