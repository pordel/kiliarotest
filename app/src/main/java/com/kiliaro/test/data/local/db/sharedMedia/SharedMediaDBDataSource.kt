package com.kiliaro.test.data.local.db.sharedMedia

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface SharedMediaDBDataSource {

    @Query("SELECT * FROM sharedMedia")
    suspend fun selectAll(): List<SharedMediaEntity>?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(sharedMedia: SharedMediaEntity): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(sharedMedia: List<SharedMediaEntity>): List<Long>

    @Query("DELETE FROM sharedMedia")
    suspend fun deleteAll()
}