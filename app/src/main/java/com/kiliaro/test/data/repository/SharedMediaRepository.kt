package com.kiliaro.test.data.repository

import android.content.Context
import com.bumptech.glide.Glide
import com.kiliaro.test.AppConstants
import com.kiliaro.test.data.base.DataState
import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaDBDataSource
import com.kiliaro.test.data.local.db.sharedMedia.SharedMediaEntity
import com.kiliaro.test.data.mapper.SharedMediaMapper
import com.kiliaro.test.data.remote.api.ApiCallHelper.Companion.safeApiCall
import com.kiliaro.test.data.base.NetworkErrorType
import com.kiliaro.test.data.remote.api.sharedMedia.SharedMediaApiDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SharedMediaRepository @Inject constructor(
    private val dbDataSource: SharedMediaDBDataSource,
    private val apiDataSource: SharedMediaApiDataSource
) {

    suspend fun getSharedMedia(offlineMode: Boolean): Flow<DataState<List<SharedMediaEntity>>> =
        flow {
            val dbData = dbDataSource.selectAll()
            if (dbData.isNullOrEmpty()) {
                if (offlineMode) {
                    emit(DataState.NetworkError(NetworkErrorType.NetworkConnection()))
                } else {
                    safeApiCall { apiDataSource.getSharedMedia(AppConstants.API_SHARED_KEY) }
                        .catch {
                            emit(DataState.LocalError())
                        }
                        .collect {
                            when (it) {
                                is DataState.Success -> {
                                    val sharedMedia = SharedMediaMapper.map(it.value)
                                    emit(DataState.Success(sharedMedia))
                                    dbDataSource.deleteAll()
                                    dbDataSource.insertAll(sharedMedia)
                                }
                                is DataState.LocalError -> {
                                    emit(DataState.LocalError())
                                }
                                is DataState.NetworkError -> {
                                    emit(DataState.NetworkError(it.networkError))
                                }
                                else -> emit(DataState.LocalError())
                            }
                        }
                }
            } else {
                emit(DataState.Success(dbData))
            }
        }
            .flowOn(Dispatchers.IO)
            .catch {
                emit(DataState.LocalError())
            }


    suspend fun clearData(context: Context): Flow<DataState<Boolean>> =
        flow {
            try {
                dbDataSource.deleteAll()
                withContext(Dispatchers.Main){
                    Glide.get(context).clearMemory()
                }
                Glide.get(context).clearDiskCache()
                emit(DataState.Success(true))
            }
            catch (e: Exception) {
                emit(DataState.Success(false))
            }
        }
            .flowOn(Dispatchers.IO)
            .catch {
                emit(DataState.Success(false))
            }

}