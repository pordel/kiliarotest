package com.kiliaro.test.data.remote.api.sharedMedia

import retrofit2.http.GET
import retrofit2.http.Path

interface SharedMediaApiDataSource {

    @GET("shared/{sharedkey}/media")
    suspend fun getSharedMedia(@Path("sharedkey") sharedkey: String): List<SharedMediaResponse>

}