package com.kiliaro.test.data.remote.api

import com.google.gson.*
import com.kiliaro.test.data.base.DataState
import com.kiliaro.test.data.base.NetworkErrorType
import com.kiliaro.test.data.remote.api.error.ErrorResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class ApiCallHelper {
    companion object {
        const val BAD_REQUEST = 400
        const val INTERNAL_SERVER = 500
        const val UNAUTHORIZED = 401
        const val NOT_FOUND = 404

        suspend fun <T> safeApiCall(
            apiCall: suspend () -> T
        ): Flow<DataState<T>> = flow {
            emit(
                try {
                    DataState.Success(apiCall.invoke())
                } catch (throwable: Throwable) {
                    when (throwable) {
                        is IOException -> DataState.NetworkError(NetworkErrorType.NetworkConnection())
                        is HttpException -> {
                            val errorResponse = extractHttpExceptions(throwable)
                            DataState.NetworkError(errorResponse)
                        }
                        else -> {
                            DataState.NetworkError(NetworkErrorType.Unknown())
                        }
                    }
                }
            )
        }

        private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
            return try {
                throwable.response()?.errorBody()?.source()?.let {
                    val body = throwable.response()?.errorBody()
                    val adapter = Gson().getAdapter(ErrorResponse::class.java)
                    adapter.fromJson(body?.string())
                }
            } catch (exception: Exception) {
                null
            }
        }

        private fun extractHttpExceptions(ex: HttpException): NetworkErrorType {
            return try {
                val body = ex.response()?.errorBody()
                val gson = GsonBuilder().create()
                val responseBody= gson.fromJson(body?.charStream(), JsonObject::class.java)
                val errorEntity = gson.fromJson(responseBody, ErrorResponse::class.java)
                when (errorEntity.httpCode.toInt()) {
                    BAD_REQUEST ->
                        NetworkErrorType.BadRequest(errorEntity.error)

                    INTERNAL_SERVER ->
                        NetworkErrorType.InternalServerError(errorEntity.error)

                    UNAUTHORIZED ->
                        NetworkErrorType.UnAuthorized(errorEntity.error)

                    NOT_FOUND ->
                        NetworkErrorType.ResourceNotFound(errorEntity.error)

                    else ->
                        NetworkErrorType.Unknown(errorEntity.error)

                }
            } catch (exception: Exception) {
                NetworkErrorType.Unknown()
            }
        }
    }
}