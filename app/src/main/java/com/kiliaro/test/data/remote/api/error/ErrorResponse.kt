package com.kiliaro.test.data.remote.api.error

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("http_code")
    val httpCode: String,

    @SerializedName("error")
    val error: String
)