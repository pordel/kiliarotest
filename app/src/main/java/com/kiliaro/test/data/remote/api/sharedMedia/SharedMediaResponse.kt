package com.kiliaro.test.data.remote.api.sharedMedia

import com.google.gson.annotations.SerializedName


data class SharedMediaResponse(
    @SerializedName("id")
    val mediaId: String,

    @SerializedName("user_id")
    val userId: String,

    @SerializedName("media_type")
    val mediaType: String,

    @SerializedName("filename")
    val filename: String,

    @SerializedName("size")
    val size: Int,

    @SerializedName("created_at")
    val createdAt: String,

    @SerializedName("taken_at")
    val takenAt: String?,

    @SerializedName("guessed_taken_at")
    val guessedTakenAt: String?,

    @SerializedName("md5sum")
    val md5sum: String,

    @SerializedName("content_type")
    val contentType: String,

    @SerializedName("video")
    val video: String?,

    @SerializedName("thumbnail_url")
    val thumbnailUrl: String,

    @SerializedName("download_url")
    val downloadUrl: String,

    @SerializedName("resx")
    val resX: Int,

    @SerializedName("resy")
    val resY: Int
)