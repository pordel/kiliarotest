Kiliaro mobile development test
The purpose of this test is to create a rough understanding of a candidate's coding skills. 
And also to have a piece of code that we can dicuss in detail that everyone is familiar with.

App description
The test is building a small mobile app that shows a few photos using the Kiliaro API. 
The app should display the "Test album" as a grid of thumbnails. 
See this as a chance to showcase your ideas of how to build a good app. 
The solution produced should be in a state where it is easy to continue adding new features. 
Not a final product that will never change. 
Third party libraries can be used and if you do, you should be able to explain why you chose to use them.

